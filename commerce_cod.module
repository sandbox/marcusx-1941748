<?php

/**
 * @file
 * Provides a Cash on Delivery payment method for Drupal commerce
 */

/**
 * Implements hook_menu().
 */
function commerce_cod_menu() {
  $items = array();

  // Add a menu item for capturing authorizations.
  $items['admin/commerce/orders/%commerce_order/payment/%commerce_payment_transaction/cod-payment'] = array(
    'title' => 'Cash',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_cod_payment_form', 3, 5),
    'access callback' => 'commerce_cod_payment_access',
    'access arguments' => array(3, 5),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
    'weight' => 2,
  );

  return $items;
}

/**
 * Implements hook_commerce_line_item_type_info().
 *
 * Define an cod_fee line-item type.
 */
function commerce_cod_commerce_line_item_type_info() {
  return array(
    'cod_fee' => array(
      'type' => 'cod_fee',
      'name' => t('Cash on delivery fee'),
      'description' => t('Line item for charging an extra fee for cash on delivery payment.'),
      'add_form_submit_value' => t('Add cod extra fee'),
      'base' => 'commerce_cod_line_item',
      'callbacks' => array(
        'configuration' => 'commerce_cod_configure_line_item',
      ),
    ),
  );
}

function commerce_cod_line_item_title($line_item) {
  return $line_item->line_item_label;
}

/**
 * Implements hook_commerce_price_component_type_info().
 */
function commerce_cod_commerce_price_component_type_info() {
  return array(
    'cod_fee' => array(
      'title' => t('COD fee'),
      'weight' => 35,
    ),
  );
}

/**
 * User access callback for cashing cheque
 */
function commerce_cod_payment_access($order, $transaction) {
  if ($transaction->payment_method == 'commerce_cod' && $transaction->status == COMMERCE_PAYMENT_STATUS_PENDING) {
    // Allow access if the user can update payments on this order.
    return commerce_payment_transaction_access('update', $transaction);
  }
  // Return FALSE if the transaction isn't with a COD and isn't pending.
  return FALSE;
}

/**
 * Form to Receive the Payment
 */
function commerce_cod_payment_form($form, &$form_state, $order, $transaction) {
  $form_state['order'] = $order;
  $form_state['transaction'] = $transaction;

  // Load and store the payment method instance for this transaction.
  $payment_method = commerce_payment_method_instance_load($transaction->instance_id);
  $form_state['payment_method'] = $payment_method;
/*
  $form['information'] = array(
    '#markup' => t('Payment received means that the cheque has been verified and cashed').'<br />'
  );
*/
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Payment received'),
  );
  return $form;
}

function commerce_cod_payment_form_submit($form, &$form_state) {
  $transaction = $form_state['transaction'];
  $order = $form_state['order'];
  $transaction->message = t('The Payment for this order has been received');
  $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
  commerce_payment_transaction_save($transaction);
  $form_state['redirect'] = 'admin/commerce/orders/'.$order->order_id.'/payment';
}

/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_cod_commerce_payment_method_info() {
  $payment_methods = array();

  $payment_methods['commerce_cod'] = array(
    'title' => t('Cash on Delivery'),
    'display_title' => t('Cash on Delivery (plus fee)'),
    'short_title' => t('Cash on Delivery'),
    'description' => t('Pay when you receive order products.'),
    'active' => TRUE,
  );

  return $payment_methods;
}

/**
 * Payment method callback: settings form.
 */
function commerce_cod_settings_form($settings = NULL) {

  $settings = (array) $settings + array(
    'information' => '',
    'extra_fee' => FALSE,
    'extra_fee_amount' => '',
    'extra_fee_display' => FALSE
  );

  $form = array();

  $form['information'] = array(
    '#type' => 'textarea',
    '#title' => t('Information'),
    '#description' => t('Information you would like to be shown to users when they select this payment method, such as delivery payment details.'),
    '#default_value' => $settings['information']
  );

  $form['extra_fee'] = array(
    '#type' => 'checkbox',
    '#title' => t('Collect an extra fee.'),
    '#description' => t('Check this box to specify an extra fee.'),
    '#default_value' => $settings['extra_fee'],
  );

  $form['extra_fee_amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount of the extra fee.'),
    '#description' => t('Give the ammount you want to use if this method is selected. Be aware that this needs to be in minor units aks cents.'),
    '#default_value' => $settings['extra_fee_amount'],
    '#states' => array(
      // Only show this field when the 'extra_fee' checkbox is enabled.
      'visible' => array(
        ':input[name="parameter[payment_method][settings][payment_method][settings][extra_fee]"]' => array('checked' => TRUE),
      ),
    ),
  );
  
  $form['extra_fee_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display the extra fee amount.'),
    '#description' => t('Check this box to display the amount of the extra fee to the customer.'),
    '#default_value' => $settings['extra_fee_display'],
    '#states' => array(
      // Only show this field when the 'extra_fee' checkbox is enabled.
      'visible' => array(
        ':input[name="parameter[payment_method][settings][payment_method][settings][extra_fee]"]' => array('checked' => TRUE),
      ),
    ),
  );

  return $form;
}

/**
 * Payment method callback: checkout form.
 */
function commerce_cod_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  $form = array();

  if (!empty($payment_method['settings']['information'])) {
    $form['commerce_cod_description'] = array(
      '#markup' => $payment_method['settings']['information']
    );
  }
  
  if (isset($payment_method['settings']['extra_fee']) && $payment_method['settings']['extra_fee']) {
    if(isset($payment_method['settings']['extra_fee_display']) && $payment_method['settings']['extra_fee_display']) {
      $fee_amount = commerce_currency_decimal_to_amount($payment_method['settings']['extra_fee_amount'], commerce_default_currency());
      $fee_amount_formatted = commerce_currency_format($fee_amount, commerce_default_currency());
      $form['commerce_cod_fee'] = array(
        '#markup' => t('An additional fee of !fee_amount will be applied.', array('!fee_amount' => $fee_amount_formatted))
      );
    }
  }

  // Need to create a dummy value to solve http://drupal.org/node/1230666
  // Probably an issue in the main commerce module
  $form['dummy'] = array(
    '#type' => 'hidden',
    '#value' => 'dummy'
  );


  return $form;
}

/**
 * Payment method callback: checkout form validation.
 */
function commerce_cod_submit_form_validate($payment_method, $pane_form, $pane_values, $order, $form_parents = array()) {

}

/**
 * Payment method callback: checkout form submission.
 */
function commerce_cod_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {
  $order->data['commerce_cod'] = $pane_values;

  if (!commerce_cod_line_item_exists($order) && $payment_method['settings']['extra_fee'] ){
    // Set the currency code.
    $default_currency_code = commerce_default_currency();

    // Create the new line item.
    $line_item = commerce_line_item_new('cod_fee', $order->order_id);
    // Wrap the line item and order to simplify manipulating their field data.
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
    // Populate the $line_item_wrapper...
    $line_item_wrapper->line_item_label = t('COD fee');
    $line_item_wrapper->quantity = 1;
    $line_item_wrapper->commerce_unit_price->amount = commerce_currency_decimal_to_amount($payment_method['settings']['extra_fee_amount'], $default_currency_code);
    $line_item_wrapper->commerce_unit_price->currency_code = $default_currency_code;
    // Set the price component of the unit price.
    $line_item_wrapper->commerce_unit_price->data = commerce_price_component_add(
      $line_item_wrapper->commerce_unit_price->value(),
      'cod_fee',
      $line_item_wrapper->commerce_unit_price->value(),
      TRUE,
      FALSE
    );
    // Save the incoming line item now so we get its ID.
    commerce_line_item_save($line_item);

    // Add it to the order's line item reference value.
    $order_wrapper->commerce_line_items[] = $line_item;

    // Save the order.
    commerce_order_save($order);
    
    // Update amount
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    $charge['amount'] = $order_wrapper->commerce_order_total->amount->value();
  }
  commerce_cod_transaction($payment_method, $order, $charge);
}

/**
 * Checks if an order contains a cod_fee line item.
 *
 * @param object $order
 *   The order object to check for a cod_fee line item.
 *
 * @return bool
 *   TRUE if the order contains a cot_fee line item.
 */
function commerce_cod_line_item_exists($order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  foreach ($order_wrapper->commerce_line_items as $line_item_wrapper) {
    // If this line item is a cod_fee line item...
    if ($line_item_wrapper->type->value() == 'cod_fee') {
      return TRUE;
    }
  }
}

/**
 * Deletes all cod_fee line items on an order.
 *
 * @param object $order
 *   The order object to delete the cod line items from.
 */
function commerce_cod_delete_cod_line_items($order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $line_item_id = NULL;
  foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
    // If this line item is a cod_fee line item...
    if ($line_item_wrapper->type->value() == 'cod_fee') {
      // Store its ID for later deletion and remove the reference from the line
      // item reference field.
      $line_item_id = $line_item_wrapper->line_item_id->value();
      $order_wrapper->commerce_line_items->offsetUnset($delta);
      break;
    }
  }
  // If we found any cod fee line items...
  if (!empty($line_item_id)) {
    // First save the order to update the line item reference field value.
    commerce_order_save($order);
    // Then delete the line items.
    commerce_line_item_delete($line_item_id);
  }
}

/**
 * Implements hook_commerce_order_update().
 *
 * Check if we are still on the cod payment method after an order update.
 * The user might have used the back button and choosen another payment method.
 * If so delete the cod_fee line-item.
 *
 * @param $order
 */
function commerce_cod_commerce_order_update($order) {
  if (isset($order->data['payment_method']) &&
    !(strpos($order->data['payment_method'], 'commerce_cod|') === 0)
  ) {
    commerce_cod_delete_cod_line_items($order);
  }
}

/**
 * Creates a cheque payment transaction for the specified charge amount.
 *
 * @param $payment_method
 *   The payment method instance object used to charge this payment.
 * @param $order
 *   The order object the payment applies to.
 * @param $charge
 *   An array indicating the amount and currency code to charge.
 */
function commerce_cod_transaction($payment_method, $order, $charge) {
  $transaction = commerce_payment_transaction_new('commerce_cod', $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->amount = $charge['amount'];
  $transaction->currency_code = $charge['currency_code'];
  $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
  $transaction->message = t('This order is waiting for the payment on delivery.');

  commerce_payment_transaction_save($transaction);
}


